# resync_mobile
2 Page Task Tracker App with a bottom navigation bar to navigate between the Add Task Screen and Task List Screen.

## Signing In
App uses Firebase Anonymous Sign-in

## Add Task
Simply type in the Title and Description in the Add Task Screen and tap on 'Save'.
Title must be filled in.

## Edit Task
On the Task List Screen, simply swipe right on the task to reveal the 'Edit' Button and swipe left to reveal the 'Delete' Button.