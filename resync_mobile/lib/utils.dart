import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

class Utils {
  // Stores date and time as DateTime object
  static DateTime toDateTime(Timestamp value) {
    if (value == null) return null;

    return value.toDate();
  }

  // Converts DateTime object to Json data
  static dynamic fromDateTimeToJson(DateTime dateTime) {
    if (dateTime == null) return null;

    return dateTime.toUtc();
  }

  // Transform the Json data to a list of Task Objects
  static StreamTransformer transformer<T>(
          T Function(Map<String, dynamic> json) fromJson) =>
      StreamTransformer<QuerySnapshot, List<T>>.fromHandlers(
        handleData: (QuerySnapshot data, EventSink<List<T>> sink) {
          final snaps = data.docs.map((doc) => doc.data()).toList();
          final objects = snaps.map((json) => fromJson(json)).toList();

          sink.add(objects);
        },
      );
}
