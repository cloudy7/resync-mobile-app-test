import 'package:flutter/material.dart';
import 'package:resync_mobile/components/rounded_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:resync_mobile/screens/home_screen.dart';
import 'package:resync_mobile/services/auth.dart';
import '../main.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final AuthService _auth = AuthService();
  bool isLoading = false;

  // To show spinner when page is still loading
  void showLoading() {
    setState(() {
      isLoading = true;
    });
  }

  // To hide spinner when page is loaded
  void hideLoading() {
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(MyApp.title),
      ),
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              // Anonymous Sign-in Button via Firebase
              RoundedButton(
                title: 'Sign In Anonymously',
                colour: Colors.teal,
                onPressed: () async {
                  showLoading();
                  try {
                    final user = await _auth.signInAnon();
                    // Pushes to the Home Screen when user signs in
                    if (user != null) {
                      hideLoading();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomeScreen()));
                      Fluttertoast.showToast(msg: 'Signed In');
                    }
                  } catch (e) {
                    print(e);
                    Fluttertoast.showToast(msg: e.toString());
                    hideLoading();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
