import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:resync_mobile/models/tasks.dart';
import 'package:resync_mobile/screens/add_task_screen.dart';
import 'package:resync_mobile/screens/tasks_list_screen.dart';
import 'package:resync_mobile/services/firebase_api.dart';
import '../main.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:resync_mobile/services/task_provider.dart';

User loggedInUser;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;
  final _auth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  // Get the user that signs in
  void getCurrentUser() {
    try {
      final User user = _auth.currentUser;
      if (user != null) {
        loggedInUser = user;
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    final tabs = [
      AddTaskScreen(),
      TaskListScreen(),
    ];

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: <Widget>[
          // Sign-out Button
          IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () {
                _auth.signOut();
                Navigator.pop(context);
                Fluttertoast.showToast(msg: 'Signed out successfully');
              })
        ],
        title: Text(MyApp.title),
      ),
      // Bottom Navigation Bar with Add Task Screen & Task List Screen
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: Colors.white.withOpacity(0.4),
        selectedItemColor: Colors.white,
        currentIndex: selectedIndex,
        onTap: (index) => setState(() {
          selectedIndex = index;
        }),
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.border_color), label: 'Add Task'),
          BottomNavigationBarItem(
              icon: Icon(Icons.assignment), label: 'Task List'),
        ],
      ),
      // Get all the Tasks from Firestore
      body: StreamBuilder<List<Task>>(
        stream: FirebaseAPI.readTasks(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            default:
              if (snapshot.hasError) {
                Fluttertoast.showToast(
                    msg: 'Something went wrong, please try again later.');
                return null;
              } else {
                final tasks = snapshot.data;
                final provider = Provider.of<TaskProvider>(context);

                // Sets all the Tasks and notifies all listeners
                provider.setTasks(tasks);
                return tabs[selectedIndex];
              }
          }
        },
      ),
    );
  }
}
