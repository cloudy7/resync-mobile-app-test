import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:resync_mobile/models/tasks.dart';
import 'package:resync_mobile/services/task_provider.dart';
import 'package:resync_mobile/widgets/task_form_widget.dart';

class EditTaskScreen extends StatefulWidget {
  final Task task;

  const EditTaskScreen({Key key, @required this.task}) : super(key: key);

  @override
  _EditTaskScreenState createState() => _EditTaskScreenState();
}

class _EditTaskScreenState extends State<EditTaskScreen> {
  // Uniquely identifies Form for validation later
  final _formKey = GlobalKey<FormState>();
  TextEditingController titleController;
  TextEditingController textController;

  String title;
  String description;

  @override
  void initState() {
    super.initState();

    // Inherits Title and Description from parent widget
    title = widget.task.title;
    description = widget.task.description;

    // Set initial value of Title and Description controller
    this.titleController = TextEditingController(text: title);
    this.textController = TextEditingController(text: description);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Task'),
        actions: [
          // Delete Icon to delete Task
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {
              // Removes Task and return to Task List Screen
              final provider =
                  Provider.of<TaskProvider>(context, listen: false);
              provider.removeTask(widget.task);

              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        // Allows the user to Edit Task
        child: Form(
          key: _formKey,
          child: TaskFormWidget(
            titleController: titleController,
            textController: textController,
            title: title,
            description: description,
            onChangedTitle: (title) => setState(() => this.title = title),
            onChangedDescription: (description) =>
                setState(() => this.description = description),
            onSavedTask: saveTask,
          ),
        ),
      ),
    );
  }

  // Validates then saves the edited Task after user input
  void saveTask() {
    final isValid = _formKey.currentState.validate();

    if (!isValid) {
      return;
    } else {
      final provider = Provider.of<TaskProvider>(context, listen: false);

      provider.updateTask(widget.task, title, description);

      Navigator.of(context).pop();
    }
  }
}
