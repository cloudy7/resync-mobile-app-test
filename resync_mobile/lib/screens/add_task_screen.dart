import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:resync_mobile/constants.dart';
import 'package:resync_mobile/models/tasks.dart';
import 'package:resync_mobile/services/task_provider.dart';
import 'package:resync_mobile/widgets/task_form_widget.dart';

class AddTaskScreen extends StatefulWidget {
  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  // Uniquely identifies Form for validation later
  final _formKey = GlobalKey<FormState>();
  String title = '';
  String description = '';
  final TextEditingController titleController = TextEditingController(text: '');
  final TextEditingController textController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 30),
          Text(
            'Add Task',
            style: kHeadingStyle,
          ),
          const SizedBox(height: 4),
          TaskFormWidget(
            titleController: titleController,
            textController: textController,
            // Allows user to input Title of Task
            onChangedTitle: (title) => setState(() => this.title = title),
            // Allows user to input Description of Task
            onChangedDescription: (description) =>
                setState(() => this.description = description),
            // Saves the Task
            onSavedTask: addTask,
          ),
        ],
      ),
    );
  }

  // Validates the Form then adds the Task
  void addTask() {
    final isValid = _formKey.currentState.validate();

    if (!isValid) {
      return;
    } else {
      final task = Task(
        id: DateTime.now().toString(),
        title: title,
        description: description,
        createdTime: DateTime.now(),
      );

      final provider = Provider.of<TaskProvider>(context, listen: false);
      provider.addTask(task);
    }
  }
}
