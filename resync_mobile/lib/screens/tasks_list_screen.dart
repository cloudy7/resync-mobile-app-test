import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:resync_mobile/constants.dart';
import 'package:resync_mobile/services/task_provider.dart';
import 'package:resync_mobile/widgets/task_widget.dart';

class TaskListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<TaskProvider>(context);
    final tasks = provider.tasks;

    // Displays a ListView of Tasks
    return tasks.isEmpty
        ? Center(
            child: Text(
              'Task List is empty',
              style: kTaskStyle,
            ),
          )
        : ListView.separated(
            physics: BouncingScrollPhysics(),
            padding: EdgeInsets.all(16),
            // Separator between each Task
            separatorBuilder: (context, index) => Container(height: 8),
            itemCount: tasks.length,
            itemBuilder: (context, index) {
              final task = tasks[index];

              return TaskWidget(task: task);
            },
          );
  }
}
