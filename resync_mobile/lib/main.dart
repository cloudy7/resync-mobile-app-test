import 'package:flutter/material.dart';
import 'package:resync_mobile/screens/welcome_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'services/task_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'Resync Task Tracker';

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => TaskProvider(),
        child: MaterialApp(
          theme: ThemeData.dark(),
          debugShowCheckedModeBanner: false,
          title: title,
          home: WelcomeScreen(),
        ),
      );
}
