import 'package:flutter/material.dart';

TextStyle kTaskStyle = TextStyle(
  fontFamily: 'WorkSans',
  fontSize: 20.0,
);

TextStyle kHeadingStyle = TextStyle(
  fontFamily: 'WorkSans',
  fontSize: 35.0,
  fontWeight: FontWeight.bold,
);

TextStyle kTitleStyle = TextStyle(
  fontFamily: 'WorkSans',
  fontSize: 22.0,
  fontWeight: FontWeight.w800,
);
