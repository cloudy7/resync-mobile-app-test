import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'firebase_api.dart';
import 'package:resync_mobile/models/tasks.dart';

class TaskProvider extends ChangeNotifier {
  List<Task> _tasks = [];
  // Getter for the list of Tasks
  List<Task> get tasks => _tasks.toList();

  // Sets the Tasks after first frame has loaded, then notify all Listeners
  void setTasks(List<Task> tasks) =>
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _tasks = tasks;
        notifyListeners();
      });

  // Adds a Task
  void addTask(Task task) => FirebaseAPI.addTask(task);

  // Removes a Task
  void removeTask(Task task) => FirebaseAPI.deleteTask(task);

  // Updates a Task
  void updateTask(Task task, String title, String description) {
    task.title = title;
    task.description = description;

    FirebaseAPI.updateTask(task);
  }
}
