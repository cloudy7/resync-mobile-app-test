import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:resync_mobile/models/tasks.dart';
import 'package:resync_mobile/utils.dart';

class FirebaseAPI {
  // Adds a Task to Firestore
  static Future<String> addTask(Task task) async {
    final docTask = FirebaseFirestore.instance.collection('task').doc();

    task.id = docTask.id;
    await docTask.set(task.toJson());

    return docTask.id;
  }

  // Reads all the Tasks from Firestore
  static Stream<List<Task>> readTasks() => FirebaseFirestore.instance
      .collection('task')
      // Sorts the Tasks with the most recent one first
      .orderBy(TaskField.createdTime, descending: true)
      .snapshots()
      .transform(Utils.transformer(Task.fromJson));

  // Updates the specific Task from Firestore
  static Future updateTask(Task task) async {
    final docTask = FirebaseFirestore.instance.collection('task').doc(task.id);

    await docTask.update(task.toJson());
  }

  // Deletes the specific Task from Firestore
  static Future deleteTask(Task task) async {
    final docTask = FirebaseFirestore.instance.collection('task').doc(task.id);

    await docTask.delete();
  }
}
