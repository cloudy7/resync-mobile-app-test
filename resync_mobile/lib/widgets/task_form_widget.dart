import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class TaskFormWidget extends StatelessWidget {
  final String title;
  final String description;
  final ValueChanged<String> onChangedTitle;
  final ValueChanged<String> onChangedDescription;
  final VoidCallback onSavedTask;
  final TextEditingController titleController;
  final TextEditingController textController;

  TaskFormWidget({
    Key key,
    this.titleController,
    this.textController,
    this.title = '',
    this.description = '',
    @required this.onChangedTitle,
    @required this.onChangedDescription,
    @required this.onSavedTask,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildTitle(),
        SizedBox(height: 12),
        buildDescription(),
        SizedBox(height: 12),
        buildButton(),
      ],
    );
  }

  // Shows the Title input field
  Widget buildTitle() {
    return TextFormField(
      maxLines: 1,
      controller: titleController,
      onChanged: onChangedTitle,
      validator: (title) {
        if (title.isEmpty) {
          return 'The title cannot be empty';
        }
        return null;
      },
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        labelText: 'Title',
        hintText: 'Enter Title',
      ),
    );
  }

  // Shows the description input field
  Widget buildDescription() {
    return TextFormField(
      maxLines: 3,
      controller: textController,
      onChanged: onChangedDescription,
      decoration: InputDecoration(
          border: UnderlineInputBorder(),
          labelText: 'Description',
          hintText: 'Enter Description'),
    );
  }

  // Shows the 'Save' Button
  Widget buildButton() {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.black),
        ),
        onPressed: () {
          // Call-back method that is passed in
          onSavedTask();
          // Clears the input fields
          titleController.clear();
          textController.clear();
          Fluttertoast.showToast(msg: "Task added!");
        },
        child: Text('Save'),
      ),
    );
  }
}
