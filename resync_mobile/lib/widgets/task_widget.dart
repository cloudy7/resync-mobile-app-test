import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:resync_mobile/constants.dart';
import 'package:resync_mobile/models/tasks.dart';
import 'package:resync_mobile/screens/edit_task_screen.dart';
import 'package:resync_mobile/services/task_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';

class TaskWidget extends StatelessWidget {
  final Task task;

  const TaskWidget({
    @required this.task,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Rounded-corners Rectangular Widget which allows Slide Action
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        key: Key(task.id),
        actions: [
          // Slides Widget to the right to show 'Edit' option
          IconSlideAction(
            color: Colors.green,
            onTap: () => editTask(context, task),
            caption: 'Edit',
            icon: Icons.edit,
          )
        ],
        secondaryActions: [
          // Slides Widget to the left to show 'Delete' option
          IconSlideAction(
            color: Colors.red,
            caption: 'Delete',
            onTap: () => deleteTask(context, task),
            icon: Icons.delete,
          )
        ],
        child: buildTask(context),
      ),
    );
  }

  // Pushes the Edit Task Screen
  void editTask(BuildContext context, Task task) => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => EditTaskScreen(task: task),
        ),
      );

  // Deletes the Task
  void deleteTask(BuildContext context, Task task) {
    final provider = Provider.of<TaskProvider>(context, listen: false);
    provider.removeTask(task);

    Fluttertoast.showToast(msg: 'Task deleted');
  }

  // Shows the Title and Description of the Task on the Widget
  Widget buildTask(BuildContext context) {
    return GestureDetector(
      onTap: () => editTask(context, task),
      child: Container(
        color: Colors.grey,
        padding: EdgeInsets.all(20),
        child: Row(children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  task.title,
                  style: kTitleStyle,
                ),
                if (task.description.isNotEmpty)
                  Container(
                    margin: EdgeInsets.only(top: 4),
                    child: Text(
                      task.description,
                      style: kTaskStyle,
                    ),
                  ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
