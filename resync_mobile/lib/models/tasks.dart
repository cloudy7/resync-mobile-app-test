import 'package:flutter/cupertino.dart';
import '../utils.dart';

class TaskField {
  static const createdTime = 'createdTime';
}

class Task {
  DateTime createdTime;
  String title;
  String id;
  String description;

  Task({
    @required this.createdTime,
    @required this.title,
    this.description = '',
    this.id,
  });

  // Converts Json data to Task object
  static Task fromJson(Map<String, dynamic> json) => Task(
        createdTime: Utils.toDateTime(json['createdTime']),
        title: json['title'],
        description: json['description'],
        id: json['id'],
      );

  // Converts Task object to Json data
  Map<String, dynamic> toJson() => {
        'createdTime': Utils.fromDateTimeToJson(createdTime),
        'title': title,
        'description': description,
        'id': id,
      };
}
